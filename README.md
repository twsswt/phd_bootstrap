# Getting Started on a PhD

## Dr. Tim Storer,<br/> School of Computing Science, University of Glasgow.

This documents details things to do in the first months of a PhD.  It's based on my interpretation of the process and
criteria for progression used in the School of Computing Science, at the University of Glasgow, as well as the advice I
give my own students for passing the first year viva. I expect it has some relevance in other institutions.

## Acknowledgements 

I can't claim credit for all or even most of the ideas.  For example, The first person who outlined the hour glass
metaphor to me was my own PhD supervisor, Ishbel Duncan, although I'm pretty sure it was passed on to her by her own
supervisor.  The idea of having a shared thesis document between students and supervisor was given to me by Babak
Esfandiari.  So thanks to everybody who has given me good ideas for managing PhD students over the years and apologies
for not name-checking all of you.

## General Advice

1. Depth versus breadth.  In my experience new PhD students *overestimate the breadth* of the contribution they are
   required to make and *underestimate the depth* to which they need to investigate a research problem.  This means
   students over estimate the amount they have to build and underestimate the extent to which they have to validate rigorously what
   they have done.

2. Don't rush in to an experiment, or writing a research paper.  Experiments are generally very expensive (in terms of
   time, as well as money) to complete and easy to get wrong.  In the early stages of a PhD focus on understanding the
   field first so that you can be confident that what you are doing is both novel and interesting.  before moving on to
   conducting the *smallest experiment possible to prove the viability of your idea.*

3. Create your PhD thesis document in the first week of your PhD, while you are doing the other settling in activities,
   and use a software project management service, such as Github or Bitbucket to manage it. This may seem strange when
   you haven't even done any research yet, let alone written a research paper.  However, just like in a software
   project, the thesis is the artifact that is the focus of effort in your PhD.  Creating the thesis as a living
   document allows you to track progress towards completion.  The change management tool allows you to receive feedback
   and suggested revisions directly to the source, rather than scribbled notes on paper.  The issue management feature
   of most of these services allow you to track on-going tasks that will contribute to the development of your thesis.
   Treating the thesis as a shared project encourages your supervisor to offer feedback on an on-going basis rather than
   when you remember to send them a PDF.  They will also always have the latest copy of your writing for review.
   <br/>
   See Change Management for your PhD Thesis for a quick guide to setting up your thesis in Github.

4. Share as much of your work as possible.  Take every opportunity to go to research meetings, talks, seminars,
   networking sessions, knowledge exchange events and so on.  This is a good thing for lots of reasons:
   <br/>
    * Reducing the risk that you are unwittingly reproducing someone else's work.  A critical success criteria for
      completing a PhD is making a novel contribution to research.  Discussing your ideas with other researchers is a
      good way to become confident that your results are novel.  Ignorance of other research is not a valid defense in a
      viva (see Conducting a Literature Review below).
    * Be better able to direct your research for real world relevance/impact.  Talking to industry or other
      organisations about the challenges they are facing can provide you with opportunities to apply your research in
      the real world (that is a good defence for research work).
	* Other people are cleverer than you and surprisingly generous when you treat them as such.  Sharing your ideas with
      another researcher may provide you with inspiration for your own research, or present an opportunity for
      collaboration.  Developing networks like this will be useful later on in your academic career.

## Writing a First Proposal

Fundamentally, the main goal of a PhD is about creating new knowledge by answering questions that *no one* has answered before.  This often involves the construction of artefacts (such as software systems) but this is very much a by-product of the main goal.  Many proposals I receive make the mistake of getting this backwards, focusing on the innovative product that they want to build, rather than the research question they want to address.

A good proposal contains:

 * Some review of the relevant literature to indicate that the student understands something about the topic and has identified a problem area.

 * A central question or set of questions that will be addressed in the course of the thesis.

 * A methodology for addressing the questions that may contain links to other work (experimental approaches, artifacts that need to be built and evaluated...).
 
When I review a proposal I look for a sense that a candidate understands what research is about, rather than a completely polished understanding of the appropriate scope and methodology for a proposal.

## Your First Year Goals

The first year of a PhD can seem very daunting, with lots of potential avenues to explore and the prospect of a
progression viva just four months away.  The purpose of the viva is to assess the likelihood of a student completing
their research within the remaining 2-2.5 years. The viva assesses four different aspects of a student's work:

1. Identify the broad area you want to work in and write an initial literature review.
   <br/>
   Purpose:

    * Start to become an expert in your chosen topic, and be able to demonstrate that you are an expert in the topic
   	  because you have studied the literature.
    * Identify more precisely the area within your broad research topic that you want to investigate during your PhD and
 	  make a convincing case that you've found a real question to answer (that hasn't been answered before).
    * Identify more precisely the area within your broad research topic that you want to investigate during your PhD and
      make a convincing case that you've found a real question to answer (that hasn't been answered before).
    * Be sure that any experiments you are conducting are novel (unless you are *explicitly* reproducing someone else's
      work).  You do not want to spend a lot of time completing an expensive experiment and a paper write up, only to
      discover that the work has already been done.
 
2. Undertake a mini-research Project.
   <br/>
   Purpose:

    * Begin to practice the methods you will employ during the rest of your PhD on a larger scale.
    * Demonstrate a capacity for conducting research.
    * Further investigate the research area you have identified to decide what further precise research questions should
      be answered.

3. Prepare a thesis statement and accompanying justification.
   <br/>  
   Purpose:
   
    * Demonstrate that you have identified a novel research contribution to be made and that you understand what is
      realistic to undertake within two years.
 
4. A plan for addressing the thesis statement over the following 2-2.5 years.
   <br/>

    * Demonstrate that you have a viable route to completion of your research project.
    * Demonstrate that you understand what is feasible and realistic in the time frame allotted.

I don't think I've ever been part of a first year review where *every* one of these goals has been successfully met.  In
particular, most students have not adjusted their thinking regarding the 'depth vs breadth' argument above.

## Writing a Progress Report

A progress report, submitted in advance of a progression viva should be used to convince the reader that you have a viable path to completing your PhD thesis.  These reports are short (~15 pages max) and should not repeat the contents of the thesis.  Instead, they should contain the following, I think (the guidance at Glasgow isn't that clear).

1. Background Reader (2-3 pages).  Give enough information to convince the reader that you are addressing an interesting problem and so that they have a good understanding of the context.

2. Thesis Statement and Research Questions (1 page).  See above.

3. Thesis Plan (1-2 pages).  Give a Chapter Title and brief outline for each thesis chapter.  Typically something like:

   1. Introduction
   2. Literature Survey
   3. Research Method
   4. Initial exploratory experiment (done in year 1).
   5. More complicated experiment (done in year 2)
   6. Another experiment looking another aspect of the problem (done in year 2/3).
   7. Speculative and more risky experiment done in a rush in year 3.
   8. Conclusions and Future Work.

4. State your achievements over the previous period and summarise the work involved, showing how they fit into your thesis (2-3 pages).

5. Describe the work packages needed over the next 12-24 months to complete the work and state how they will contribute to the thesis (research questions and chapterS) (2-3 pages).

6. State any significant, non-generic risks and how you are mitigating them (1 page).  E.g. do you depend on an external project partner.

## Tools you will need to know about

So find some tutorials online!  There are plenty of good ones.

 * A change management tool and hosting service, e.g. GitHub for Git, or Bitbucket for Git, Subversion and Mecurial.
 * LaTeX for document preparation.
 * Make, Ant or Maven for compiling your thesis PDF from the LaTeX source.
 * Travis or Jenkins for Continuous Integration.
 * Mendeley or similar, for bibliography management.

## Change Management for your PhD Thesis

1. Create a Github, bitbucket, or similar hosted change management account.  Choose a service that supports
   fully-fledged change management tools like Subversion, Git or Mercurial.  Dropbox or other file synchronisation tools
   don't count.  If you are worried about sharing your thesis publicly then you can use a private repository (Bitbucket
   offers this for free, Github makes you pay)
2. Clone Stephen Strowe's LaTeX [thesis template](https://github.com/sdstrowes/Glasgow-Thesis-Template)) for Glasgow
   University and host the clone in your GitHub account.
3. Clone the repository in your account onto as many of your local devices as you like.
4. Edit the meta-data in the thesis (author name, thesis title).  This will make you feel like you own it.  Commit the
   changes back to your hosted repository.
5. Offer to share the repository URL with your supervisor.  This is an effective way of receiving comments (as pull
   requests or issues).
6. Commit *everything* you write about to the thesis document you've created.  Do these commits regularly (at least once
   a day) even if it is just notes.  Don't put your notes into private Word documents, or other text documents that
   don't get included in the repository.
          
## Writing a Literature Review

A literature review can seem daunting, especially if English (the defacto standard of scientific communication, whether
you like it or not) is your first language.  Here is my recommendation for creating a literature review in a
semi-systematic way (I'm not a fan of systematic literature reviews for Computing Science, the field is too
heterogeneous for the method to be plausible).

 1. Create a chapter in your thesis called 'Literature Survey'.
 2. You forgot to do a commit of the new LaTeX file.  Go back and do that now.
 3. Decide on some initial search terms and try them out in Google Scholar, Elsevier, DBLP....  Write these down in your
   thesis (and commit). Be prepared to adjust your search terms as new related topics are discovered (and be disciplined
   about documenting your changes in your thesis).
 4. Conduct a search using your search terms and store the results in a management tool such as Mendeley.
 5. Decide on inclusion and exclusion criteria.  That's right, write them down and commit (you can always change them
   later).
 6. Read the papers you have found.
    1. Start by reading the abstract, introduction and conclusions.  Decide if the paper is sufficiently relevant to
       read the whole thing.  If it is relevant and meets your inclusion criteria, skim read the whole document once.
       Do *not* pause if there is a point where you don't understand.  This may seem odd, but often you will get a
       better understanding of a particular point if you have read the whole paper.  Now, if the paper is really
       critical to
      your work, then re-read it carefully.
    2. Create an annotated bibliography entry in your thesis for each paper that you read containing answers to the
       following sentences.
        * What did the authors seek to discover/learn/prove in their research?  What was their research question?
        * What was their methodology?
        * What were their conclusions?
    3. You may also find you can add discussion points on (this will happen the more you read):
        * Weaknesses in the paper - is there a undocumented limitation that you have found?
        * Relationships to other papers you have already read.
    4. Conduct backward and forward *snowballing* to find relevant papers that were cited and that cited the paper you
       are currently reading.  Add these papers into your reference manager and continue.
   
7. Don't be concerned if you start off only reading a few papers per day (perhaps only 1). Reading papers takes practice
   and as you become more familiar with your field, you'll start to identify the signposts in the paper that make
   reading easier.  As a rule of thumb you should expect to read between 100 and 200 papers for the initial draft of
   your literature review and you should allow 2-3 months for this.

8. As you start to build up a corpus of findings from step (6) start thinking about synthesis.  What are the common
   themes in the research area?  Where are the most recent authors talking about plans for future work?  Where is the
   field moving to?  Start trying to group your paragraphs about individual papers into related themes.  Gradually build
   an argument about the field, the limitations and the important research directions in the future.

9. If you find yourselve struggling to make progress on reading then it's okay to step away from the literature for a
   while.  One of my students talked about a literature review as like drinking water: you get the most benefit if you
   savour it in small sips, rather than trying to guzzle.

